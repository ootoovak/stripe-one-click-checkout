source "https://rubygems.org"

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?("/")
  "https://github.com/#{repo_name}.git"
end

gem "rails", "~> 5.2.1"
gem "sqlite3"
gem "puma", "~> 3.7"
gem "sass-rails", "~> 5.0"
gem "uglifier", ">= 1.3.0"
gem "coffee-rails", "~> 4.2"
gem "bcrypt"
gem "pundit"
gem "bootsnap", ">= 1.1.0", require: false
gem "stripe"

group :development, :test do
  gem "byebug", platforms: [:mri, :mingw, :x64_mingw]
  gem "database_cleaner"
  gem "rspec-rails"
  gem "factory_bot_rails"
  gem "capybara"
  gem "selenium-webdriver"
  gem "fuubar"
  gem "simplecov", require: false
end

group :development do
  gem "web-console", ">= 3.3.0"
  gem "listen", ">= 3.0.5", "< 3.2"
  gem "spring"
  gem "spring-watcher-listen", "~> 2.0.0"
end

group :test do
  # Using this branch until Stripe Checkout is properly supported
  # PR here: https://github.com/rebelidealist/stripe-ruby-mock/pull/648
  gem "stripe-ruby-mock", github: "fauxparse/stripe-ruby-mock", branch: "support_checkout_sessions"
  gem "webdrivers"
  gem "launchy"
end

gem "tzinfo-data", platforms: [:mingw, :mswin, :x64_mingw, :jruby]

# Stripe One Click Checkout

An example app demonstrating the usage of Stripe Checkout to achieve a one-click checkout for customers.

## Install

Run the following to install the required libraries.

```bash
bundle install
```

Run the following to setup the database.

```bash
bin/setup
```

## Run Application

```bash
bin/rails server
```


module Posts
  class PurchasesController < ApplicationController
    before_action :login_required

    def create
      result = StartPurchaseService.call(current_user,
        post_id: params[:post_id],
        success_url: stripe_checkout_success_url(params[:post_id]),
        cancel_url: stripe_checkout_cancellation_url(params[:post_id])
      )

      if result.ok?
        render json: { sessionId: result.session_id }
      else
        redirect_to post_url(params[:post_id]), notice: t(".purchase_error")
      end
    end
  end
end
module Stripe
  module Checkout
    class CancellationsController < ApplicationController
      before_action :login_required

      def show
        redirect_to post_url(params[:id]), alert: t(".cancelled")
      end
    end
  end
end

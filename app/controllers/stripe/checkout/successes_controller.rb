module Stripe
  module Checkout
    class SuccessesController < ApplicationController
      before_action :login_required

      def show
        redirect_to post_url(params[:id]), notice: t(".successful")
      end
    end
  end
end
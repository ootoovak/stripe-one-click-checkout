module Stripe
  module Checkout
    class WebhooksController < ApplicationController
      skip_before_action :verify_authenticity_token

      def create
        result = ProcessPaymentService.call(request.raw_post,
          secret: Rails.application.secrets.stripe_secret_webhook_key,
          signature: request.headers["HTTP_STRIPE_SIGNATURE"]
        )

        if result
          head :ok
        else
          head :bad_request
        end
      end
    end
  end
end
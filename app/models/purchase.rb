class Purchase < ApplicationRecord
  belongs_to :user
  belongs_to :post

  validates :user, presence: true
  validates :post, presence: true

  scope :post_paid, ->(post) { order(created_at: :desc).where(post: post, paid: true) }

  def paid!
    self.paid = true
    save
  end
end

class PostPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope
    end
  end

  def create?
    user.present?
  end

  def update?
    record.author == user
  end

  def read?
    CheckUserPostPaymentService.call(user, record)
  end
end

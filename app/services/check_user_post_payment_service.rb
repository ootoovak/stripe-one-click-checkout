class CheckUserPostPaymentService < ApplicationService
  def initialize(user, post)
    @user = user
    @post = post
  end

  def call
    return false unless user.present?
    return true if post.author == user
    user.purchases.post_paid(post).first.present?
  end

  private

  attr_reader :user, :post
end
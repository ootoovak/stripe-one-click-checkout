class ProcessPaymentService < ApplicationService
  COMPLETED_CHECKOUT = "checkout.session.completed".freeze

  def initialize(data, secret:, signature:)
    @data = data
    @secret = secret
    @signature = signature
  end

  def call
    event = Stripe::Webhook.construct_event(data, signature, secret)
    return false unless event.type == COMPLETED_CHECKOUT

    checkout_session_id = event&.data&.object&.id
    purchase = Purchase.find_by(checkout_session_id: checkout_session_id)
    return false unless purchase.present?

    purchase.paid!
  rescue JSON::ParserError, Stripe::SignatureVerificationError
    false
  end

  private

  attr_reader :data, :secret, :signature
end
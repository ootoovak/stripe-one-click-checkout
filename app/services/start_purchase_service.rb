class StartPurchaseService < ApplicationService
  Result = Struct.new(:session_id) do
    def ok?
      session_id.present?
    end
  end

  def initialize(user, post_id:, success_url:, cancel_url:)
    @user = user
    @post = Post.find(post_id)
    @success_url = success_url
    @cancel_url = cancel_url
  end

  def call
    return Result.new(nil) unless post.present?

    @purchase = user.purchases.create(post: post)
    session = create_stripe_session
    purchase.checkout_session_id = session.id

    return Result.new(nil) unless purchase.save

    Result.new(session.id)
  end

  private

  attr_reader :user, :post, :purchase, :cancel_url

  def create_stripe_session
    Stripe::Checkout::Session.create(
      success_url: success_url,
      cancel_url: cancel_url,
      payment_method_types: ["card"],
      line_items: [{
        name: post.title,
        quantity: 1,
        currency: "aud",
        amount: 100,
        description: post.attribution
      }],
    )
  end

  def success_url
    @success_url + "?session_id={CHECKOUT_SESSION_ID}"
  end
end
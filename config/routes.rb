Rails.application.routes.draw do
  root "posts#index"

  get "/login", to: "login#new"
  post "/login", to: "login#create"
  delete "/logout", to: "login#destroy"


  resources :posts, except: :index do
    resources :purchases, only: [:create], module: :posts
  end

  namespace :stripe do
    namespace :checkout do
      resources :successes, only: [:show]
      resources :cancellations, only: [:show]
      resources :webhooks, only: [:create]
    end
  end
end
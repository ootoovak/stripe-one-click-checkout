class CreatePurchases < ActiveRecord::Migration[5.2]
  def change
    create_table :purchases do |t|
      t.references :user, foreign_key: true
      t.references :post, foreign_key: true
      t.string :checkout_session_id
      t.boolean :paid, default: false

      t.timestamps
    end
  end
end

require "rails_helper"

module Posts
  RSpec.describe PurchasesController do
    let(:user) { instance_double(User) }

    before do
      allow_any_instance_of(ApplicationController).to receive(:current_user).and_return(user)
    end

    describe "POST create" do
      context "when a purchase has been successfully started" do
        render_views

        it "renders the expected JSON response" do
          result = double("result", ok?: true, session_id: "sessionUuid")
          allow(StartPurchaseService).to receive(:call).and_return(result)

          post :create, params: { post_id: 88 }

          aggregate_failures do
            expect(StartPurchaseService).to have_received(:call).with(user, {
              post_id: "88",
              success_url: "http://test.host/stripe/checkout/successes/88",
              cancel_url: "http://test.host/stripe/checkout/cancellations/88"
            })
            expect(response.content_type).to eq("application/json")
            expect(response.body).to eq("{\"sessionId\":\"sessionUuid\"}")
          end
        end
      end

      context "when a purchase has not been successfully started" do
        it "redirects to the post page" do
          result = double("result", ok?: false)
          allow(StartPurchaseService).to receive(:call).and_return(result)

          post :create, params: { post_id: 88 }

          expect(response).to redirect_to(post_url(88))
        end
      end
    end
  end
end
require "rails_helper"

module Stripe
  module Checkout
    RSpec.describe CancellationsController do
      let(:user) { instance_double(User) }

      before do
        allow_any_instance_of(ApplicationController).to receive(:current_user).and_return(user)
      end

      describe "GET show" do
        it "redirects back to the post the user was looking at" do
          get :show, params: { id: 99 }
          expect(response).to redirect_to(post_url(99))
        end
      end
    end
  end
end
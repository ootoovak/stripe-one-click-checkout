require "rails_helper"

module Stripe
  module Checkout
    RSpec.describe WebhooksController do
      describe "POST create" do
        context "when a payment has been successfully processed" do
          it "returns a success status" do
            allow(ProcessPaymentService).to receive(:call).and_return(true)

            request.headers["HTTP_STRIPE_SIGNATURE"] = "aSignatureFromStripe"
            post :create, params: { some: "data" }

            aggregate_failures do
              expect(ProcessPaymentService).to have_received(:call).with("some=data", {
                secret: "whsec_XksOVZD8dxogna4b8ol9R65CoAhdVvkS",
                signature: "aSignatureFromStripe"
              })
              expect(response).to have_http_status(:ok)
            end
          end
        end

        context "when a payment has not been successfully processed" do
          it "returns a bad request status" do
            allow(ProcessPaymentService).to receive(:call).and_return(false)

            request.headers["HTTP_STRIPE_SIGNATURE"] = "aSignatureFromStripe"
            post :create, params: { some: "data" }

            expect(response).to have_http_status(:bad_request)
          end
        end
      end
    end
  end
end
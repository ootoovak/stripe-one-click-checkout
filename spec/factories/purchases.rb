FactoryBot.define do
  factory :purchase do
    user
    post
    checkout_session_id { "cs_test_000SomeRandomCharacterString000" }
    paid { false }
  end
end

require "rails_helper"

RSpec.describe "full post purchases", type: :feature, js: true do
  before { StripeMock.start }
  after { StripeMock.stop }

  let(:body) { File.read("spec/test_data/posts/body_1.txt") }
  let(:title) { "Fake post from loripsum.net" }
  let(:first_sentence) { "Lorem ipsum dolor sit amet, consectetur adipiscing elit." }
  let(:last_sentence) { "Tu autem inter haec tantam multitudinem hominum interiectam non vides nec laetantium nec dolentium?" }
  let(:author) { create(:author) }
  let(:post) { author.posts.create(title: title, body: body) }

  it "shows a preview of post for users that are not logged in asking the user to purchase access to the full post" do
    visit post_path(post)

    aggregate_failures do
      expect(page).to have_content(title)
      expect(page).to have_content(first_sentence)
      expect(page).not_to have_content(last_sentence)

      expect(page).to have_content("Payment")
      expect(page).to have_content("To continue reading this article in full you will need to log in and purchase it.")
      expect(page).to have_link("Login")
    end
  end

  it "shows a preview of the post to logged in users asking the user to purchase access to the full post" do
    user = create(:user)
    login_as(user)

    visit post_path(post)

    aggregate_failures do
      expect(page).to have_content(title)
      expect(page).to have_content(first_sentence)
      expect(page).not_to have_content(last_sentence)

      expect(page).to have_content("Payment")
      expect(page).to have_content("To continue reading this article in full you will need to log in and purchase it.")
      expect(page).to have_link("Purchase")
    end
  end

  it "shows the full post to users who have purchased access to the full post" do
    user = create(:user)
    create(:purchase, user: user, post: post, paid: true)
    login_as(user)

    visit post_path(post)

    aggregate_failures do
      expect(page).to have_content(title)
      expect(page).to have_content(first_sentence)
      expect(page).to have_content(last_sentence)

      expect(page).to_not have_content("Payment")
      expect(page).to_not have_content("To continue reading this article in full you will need to log in and purchase it.")
      expect(page).to_not have_link("Purchase")
    end
  end

  it "shows authors the full post" do
    login_as(post.author)

    visit post_path(post)

    aggregate_failures do
      expect(page).to have_content(title)
      expect(page).to have_content(first_sentence)
      expect(page).to have_content(last_sentence)

      expect(page).to_not have_content("Payment")
      expect(page).to_not have_content("To continue reading this article in full you will need to log in and purchase it.")
      expect(page).to_not have_link("Purchase")
    end
  end
end
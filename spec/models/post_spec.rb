require "rails_helper"

RSpec.describe Post do
  it "provides attribution for the post" do
    post = build(:post)
    expect(post.attribution).to eq("by John Smith")
  end
end
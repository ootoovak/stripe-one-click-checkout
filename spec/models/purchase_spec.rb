require "rails_helper"

RSpec.describe Purchase do
  it "marks a purchase as paid" do
    purchase = build(:purchase, paid: false)
    allow(purchase).to receive(:save).and_return(true)

    result = purchase.paid!

    aggregate_failures do
      expect(purchase.paid).to eq(true)
      expect(purchase).to have_received(:save)
      expect(result).to eq(true)
    end
  end
end
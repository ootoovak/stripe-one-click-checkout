require "rails_helper"

RSpec.describe PostPolicy do
  describe "can a post be created" do
    it "will allow users to create a post" do
      user = build(:user)
      post = build(:post)

      policy = PostPolicy.new(user, post)

      expect(policy.create?).to eq(true)
    end

    it "will not allow guest to create a post" do
      post = build(:post)

      policy = PostPolicy.new(nil, post)

      expect(policy.create?).to eq(false)
    end
  end

  describe "can a post be updated" do
    it "will allow authors to update a post" do
      user = build(:user)
      post = build(:post, author: user)

      policy = PostPolicy.new(user, post)

      expect(policy.update?).to eq(true)
    end

    it "will not allow anyone else to update a post" do
      user = build(:user)
      post = build(:post)

      policy = PostPolicy.new(user, post)

      expect(policy.update?).to eq(false)
    end
  end

  describe "can a post be read" do
    it "will allow users who have paid to read a post" do
      user = build(:user)
      post = build(:post)
      allow(CheckUserPostPaymentService).to receive(:call).and_return(true)

      policy = PostPolicy.new(user, post)

      aggregate_failures do
        expect(policy.read?).to eq(true)
        expect(CheckUserPostPaymentService).to have_received(:call).with(user, post)
      end
    end

    it "will not allow users who haven't paid to read a post" do
      user = build(:user)
      post = build(:post)
      allow(CheckUserPostPaymentService).to receive(:call).and_return(false)

      policy = PostPolicy.new(user, post)

      aggregate_failures do
        expect(policy.read?).to eq(false)
        expect(CheckUserPostPaymentService).to have_received(:call).with(user, post)
      end
    end
  end
end
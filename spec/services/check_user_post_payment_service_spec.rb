require "rails_helper"

RSpec.describe CheckUserPostPaymentService do
  describe "check if a user has paid to read a post" do
    it "must be given a user" do
      post = build(:post)

      service = CheckUserPostPaymentService.new(nil, post)

      expect(service.call).to eq(false)
    end

    it "is automatically considered true if the user is also the author of the post" do
      user = build(:user)
      post = build(:post, author: user)

      service = CheckUserPostPaymentService.new(user, post)

      expect(service.call).to eq(true)
    end

    it "is false if the user has not yet paid" do
      post = create(:post)
      purchase = create(:purchase, post: post, paid: false)
      user = create(:user, purchases: [ purchase ])

      service = CheckUserPostPaymentService.new(user, post)

      expect(service.call).to eq(false)
    end

    it "is false if the user has not yet paid" do
      post = create(:post)
      purchase = create(:purchase, post: post, paid: true)
      user = create(:user, purchases: [ purchase ])

      service = CheckUserPostPaymentService.new(user, post)

      expect(service.call).to eq(true)
    end
  end
end
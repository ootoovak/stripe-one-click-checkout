require "rails_helper"

RSpec.describe ProcessPaymentService do
  let(:data) { double("data") }
  let(:secret) { "sharedSecretKey" }
  let(:signature) { "providedSignature" }

  describe "processing a payment" do
    it "returns false if the JSON provided isn't valid" do
      allow(Stripe::Webhook).to receive(:construct_event).and_raise(JSON::ParserError)

      service = ProcessPaymentService.new(data, secret: secret, signature: signature)

      aggregate_failures do
        expect(service.call).to eq(false)
        expect(Stripe::Webhook).to have_received(:construct_event).with(data, signature, secret)
      end
    end

    it "returns false if the Signature can not be verified" do
      allow(Stripe::Webhook).to receive(:construct_event).and_raise(
        Stripe::SignatureVerificationError.new("Failure", signature)
      )

      service = ProcessPaymentService.new(data, secret: secret, signature: signature)

      aggregate_failures do
        expect(service.call).to eq(false)
        expect(Stripe::Webhook).to have_received(:construct_event).with(data, signature, secret)
      end
    end

    it "returns false if it isn't the right type of event" do
      event = double("event", type: "not.a.checkout")
      allow(Stripe::Webhook).to receive(:construct_event).and_return(event)

      service = ProcessPaymentService.new(data, secret: secret, signature: signature)

      aggregate_failures do
        expect(service.call).to eq(false)
        expect(Stripe::Webhook).to have_received(:construct_event).with(data, signature, secret)
      end
    end

    it "returns false if a pre-existing purchase can not be found" do
      event = double("event", type: "checkout.session.completed", data: nil)
      allow(Stripe::Webhook).to receive(:construct_event).and_return(event)
      allow(Purchase).to receive(:find_by).and_return(nil)

      service = ProcessPaymentService.new(data, secret: secret, signature: signature)

      aggregate_failures do
        expect(service.call).to eq(false)
        expect(Stripe::Webhook).to have_received(:construct_event).with(data, signature, secret)
        expect(Purchase).to have_received(:find_by).with(checkout_session_id: nil)
      end
    end

    it "returns true if a payment has been found and marked as paid" do
      id = "providedCheckoutId"
      event = double("event", type: "checkout.session.completed", data: double("data", object: double("object", id: id)) )
      allow(Stripe::Webhook).to receive(:construct_event).and_return(event)
      purchase = instance_double(Purchase, paid!: true)
      allow(Purchase).to receive(:find_by).and_return(purchase)

      service = ProcessPaymentService.new(data, secret: secret, signature: signature)

      aggregate_failures do
        expect(service.call).to eq(true)
        expect(Stripe::Webhook).to have_received(:construct_event).with(data, signature, secret)
        expect(Purchase).to have_received(:find_by).with(checkout_session_id: id)
      end
    end
  end
end
require "rails_helper"

RSpec.describe StartPurchaseService do
  let(:success_url) { "http:/test.host/success" }
  let(:cancel_url) { "http:/test.host/success" }

  describe "starting a post purchase" do
    it "can't start a purchase if the post doesn't exist" do
      user = instance_double(User)
      allow(Post).to receive(:find).and_return(nil)
      service = StartPurchaseService.new(user,
        post_id: 88, success_url: success_url, cancel_url: cancel_url
      )

      result = service.call

      aggregate_failures do
        expect(result.ok?).to eq(false) 
        expect(Post).to have_received(:find).with(88) 
      end
    end

    it "is not successful if a purchase can not be created" do
      purchase = instance_double(Purchase, save: false, :"checkout_session_id=" => "sessionId")
      purchases = double("purchases", create: purchase)
      user = instance_double(User, purchases: purchases)
      post = create(:post)
      session = double("session", id: "sessionId")
      allow(Stripe::Checkout::Session).to receive(:create).and_return(session)
      service = StartPurchaseService.new(user,
        post_id: post.id, success_url: success_url, cancel_url: cancel_url
      )

      result = service.call

      aggregate_failures do
        expect(result.ok?).to eq(false) 
        expect(Stripe::Checkout::Session).to have_received(:create).with(
          success_url: "http:/test.host/success?session_id={CHECKOUT_SESSION_ID}",
          cancel_url: "http:/test.host/success",
          payment_method_types: ["card"],
          line_items: [{
            name: post.title,
            quantity: 1,
            currency: "aud",
            amount: 100,
            description: post.attribution
          }]
        )
      end
    end

    it "is successful if a purchase is saved with a session id" do
      user = create(:user)
      post = create(:post)
      session = double("session", id: "sessionId")
      allow(Stripe::Checkout::Session).to receive(:create).and_return(session)
      service = StartPurchaseService.new(user,
        post_id: post.id, success_url: success_url, cancel_url: cancel_url
      )

      result = service.call

      aggregate_failures do
        expect(result.ok?).to eq(true) 
        expect(result.session_id).to eq("sessionId") 
        expect(user.purchases.find_by(checkout_session_id: "sessionId")).to be_present
      end
    end
  end
end